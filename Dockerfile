FROM rust:1.42.0-stretch as builder

RUN apt-get update && apt-get -y install ca-certificates cmake musl-tools pkg-config libssl-dev && rm -rf /var/lib/apt/lists/*

COPY . .
RUN cargo build --release

FROM rust:1.42.0-slim-stretch 

COPY --from=builder /target/release/enlaps_triage .

EXPOSE 6000

CMD ["/enlaps_triage"]
