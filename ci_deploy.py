import boto3, operator, os, logging
from functools import reduce
logging.getLogger().setLevel(logging.INFO)

deploy_env = os.environ.get("CI_COMMIT_REF_SLUG")

if deploy_env not in ['master']:
    logging.info("Branch '%s' is not supposed to be deployed...", deploy_env)
    quit()

logging.info("Starting deployment of environment '%s' ...", deploy_env)

autoscaling_client = boto3.client('autoscaling')
ec2_client = boto3.client('ec2')

logging.info("Fetching Autoscaling groups...")

raw_autoscaling_infos = autoscaling_client.describe_auto_scaling_groups(
    AutoScalingGroupNames=[
        'triage_application'
    ]
)

groups_list = raw_autoscaling_infos["AutoScalingGroups"]
instances_infos = map( lambda group_infos: group_infos["Instances"],groups_list )
instance_infos_single_list = reduce( operator.concat, instances_infos )
instances_ids = list(map( lambda instance_info: instance_info["InstanceId"], instance_infos_single_list ))

logging.info("Rebooting instances :  %s", ' ,'.join(instances_ids))

ec2_client.reboot_instances(
    InstanceIds = instances_ids,
    DryRun = False
)

logging.info("Finished deployment of environment '%s' ...", deploy_env)
