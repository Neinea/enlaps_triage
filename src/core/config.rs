use std::env;

use sqlx::postgres::PgPool;
use sqlx::{PgConnection, Pool};

#[derive(Debug, Clone)]
pub struct AppConfig {
    pub gitlab_base_url: String,
    pub slack_base_url: String,
    pub gitlab_access_token: String,
    pub slack_access_token: String,
    pub postgres_pool: Pool<PgConnection>,
}

impl AppConfig {
    pub async fn new() -> Self {
        AppConfig {
            gitlab_base_url: match env::var("gitlab_base_url") {
                Ok(val) => val,
                Err(_e) => panic!("The environment variable gitlab_base_url must be defined to set the application")
            },
            slack_base_url: match env::var("slack_base_url") {
                Ok(val) => val,
                Err(_) => "".to_owned()
            },
            gitlab_access_token: match env::var("gitlab_access_token") {
                Ok(val) => val,
                Err(_) => panic!("The environment variable gitlab_access_token must be defined to set the application")
            },
            slack_access_token: match env::var("slack_access_token") {
                Ok(val) => val,
                Err(_) => panic!("The environment variable slack_access_token must be defined to set the application")
            },
            postgres_pool: match env::var("postgres_pool") {
                Ok(val) => PgPool::builder().max_size(5).build(val.as_str()).await.unwrap(),
                Err(_) => panic!("The environment variable postgres_pool must be defined to set the application")
            }
        }
    }
}
