use actix_web::{post, get, error, HttpResponse, web, Responder};
use futures::StreamExt;
use serde_json::{Value, Map};

use super::action::Action;
use super::config::AppConfig;

#[post("/")]
pub async fn gitlab_webhook(mut payload: web::Payload, data: web::Data<AppConfig>) -> impl Responder {
    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk?;
        if (body.len() + chunk.len()) > 262_144 {
            return Err(error::ErrorBadRequest("overflow"))
        }
        body.extend_from_slice(&chunk);
    }
    let parsed: Value = serde_json::from_slice(&body)?;
    let obj_json: Map<String, Value> = parsed.as_object().unwrap().clone();
    println!("new webhook is coming: {}", serde_json::to_string_pretty(&parsed).unwrap());
    let action = Action::new(&obj_json, &data);
    let response = action.notify().await;
    response.await
}

#[get("/health_check")]
pub async fn health_check() -> impl Responder {
    HttpResponse::Ok()
}
