use super::shared::GitlabUser;

use sqlx::postgres::PgQueryAs;
use sqlx::{PgConnection, Pool};

pub struct SqlQuery<'a> {
    pool: &'a Pool<PgConnection>,
}

impl<'a> SqlQuery<'a> {
    pub fn new(pool: &'a Pool<PgConnection>) -> Self {
        SqlQuery { pool }
    }

    pub async fn get_random_user(&self, roles: &[String], gitlab_id: u32) -> Result<GitlabUser, sqlx::error::Error> {
        let mut conn = self.pool.acquire().await.unwrap();
        let query = build_get_random_user_query(roles, gitlab_id);
        sqlx::query_as::<_, GitlabUser>(query.as_str())
            .fetch_one(&mut conn)
            .await
    }
}

fn build_get_random_user_query(roles: &[String], gitlab_id: u32) -> String {
    let formated_roles: Vec<String> = roles.iter().map(|role| format!("'{}'", role)).collect();
    match roles.len() {
        0 => format!(
            "select * from users where gitlab_id != {} order by RANDOM() LIMIT 1;",
            gitlab_id
        ),
        _ => format!(
            "select * from users where role in ({}) and gitlab_id != {} order by RANDOM() LIMIT 1;",
            formated_roles.join(","),
            gitlab_id
        ),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_build_get_random_user_query_with_no_roles() {
        assert_eq!(
            build_get_random_user_query(&[], 12),
            String::from("select * from users where gitlab_id != 12 order by RANDOM() LIMIT 1;")
        )
    }

    #[test]
    fn test_build_get_random_user_query_with_roles() {
        assert_eq!(build_get_random_user_query(&[String::from("fullstack"), String::from("backend")], 12), String::from("select * from users where role in ('fullstack','backend') and gitlab_id != 12 order by RANDOM() LIMIT 1;"))
    }
}
