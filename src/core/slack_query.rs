use actix_web::client::Client;
use url::form_urlencoded;

pub struct SlackQuery<'a> {
    pub slack_base_url: &'a str,
    pub slack_access_token: &'a str,
}

impl<'a> SlackQuery<'a> {
    pub fn new(slack_base_url: &'a str, slack_access_token: &'a str) -> Self {
        SlackQuery {
            slack_base_url,
            slack_access_token,
        }
    }

    pub async fn ping_user_for_new_merge_request(
        &self,
        merge_request_link: &str,
        slack_id: &str,
        slack_name: &str,
    ) {
        let client = Client::default();
        let text = format!(
          "hey @{}, you have been assigned randomly to review this merge request {} as soon as it's ready !",
          slack_name,
          merge_request_link
        );

        let encoded = form_urlencoded::Serializer::new(String::new())
            .append_pair("token", self.slack_access_token)
            .append_pair("channel", slack_id)
            .append_pair("text", text.as_str())
            .finish();

        client
            .post(format!(
                "{}/api/chat.postMessage?{}",
                self.slack_base_url, encoded
            ))
            .header("Content-Type", "application/json")
            .send()
            .await
            .unwrap();
    }
}
