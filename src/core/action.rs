use super::{shared::ObjectKind, slack_query::SlackQuery, sql_query::SqlQuery};
use actix_web::client::Client;
use actix_web::{body, web, HttpResponse};
use log::{error, info};
use regex::Regex;
use serde_json::{json, Map, Value};
use url::form_urlencoded;

use super::config::AppConfig;
use std::convert::TryInto;

#[derive(Debug)]
pub struct Action<'a> {
    config: &'a web::Data<AppConfig>,
    json: &'a Map<String, Value>,
    object_kind: ObjectKind,
}

impl<'a> Action<'a> {
    pub fn new(json: &'a Map<String, Value>, config: &'a web::Data<AppConfig>) -> Self {
        let object_kind: &str = json["object_kind"].as_str().unwrap();
        match object_kind {
            "merge_request" => Action {
                object_kind: ObjectKind::MergeRequest,
                json,
                config,
            },
            "issue" => Action {
                object_kind: ObjectKind::Issue,
                json,
                config,
            },
            _ => Action {
                object_kind: ObjectKind::Unknown,
                json,
                config,
            },
        }
    }

    pub async fn notify(&self) -> HttpResponse {
        if self.json["object_attributes"].get("action").is_none() {
            error!("no action has been found on this request");
            return HttpResponse::BadRequest().finish();
        }
        let action = self.json["object_attributes"]["action"].as_str().unwrap();
        let project_id = self.extract_project_id();
        let client = Client::default();
        match self.object_kind {
            ObjectKind::MergeRequest => {
                let merge_request_iid = self.extract_merge_request_iid();
                match action {
                    "open" => {
                        self.open_merge_request(client, project_id, merge_request_iid)
                            .await
                    }
                    "update" => {
                        self.update_merge_request(client, project_id, merge_request_iid)
                            .await
                    }
                    "merge" => self.merge_merge_request(client, project_id).await,
                    _ => {
                        info!(
                            "unmanaged action called #{} linked to merge request",
                            action
                        );
                        HttpResponse::BadRequest().finish()
                    }
                }
            }
            ObjectKind::Issue => {
                let issue_iid = self.extract_issue_iid();
                match action {
                    "update" => self.update_issue(client, project_id, issue_iid).await,
                    _ => {
                        info!("unmanaged action called #{} linked to issue", action);
                        HttpResponse::BadRequest().finish()
                    }
                }
            }
            ObjectKind::Unknown => {
                info!("Currently, Enlaps triage manage only issue and merge request");
                HttpResponse::BadRequest().finish()
            }
        }
    }

    async fn merge_merge_request(&self, client: Client, project_id: i64) -> HttpResponse {
        let issue_number = self.extract_issue_number_from_description();
        let mut get_issue_response = client
            .get(format!(
                "{}/api/v4/projects/{}/issues/{}?private_token={}",
                self.config.gitlab_base_url,
                project_id,
                issue_number,
                self.config.gitlab_access_token
            ))
            .header("Content-Type", "application/json")
            .send()
            .await
            .unwrap();

        if get_issue_response.status().is_client_error()
            || get_issue_response.status().is_server_error()
        {
            error!(
                "A {:?} http code error occur to get issue from merge request",
                get_issue_response.status()
            );
            return HttpResponse::with_body(
                get_issue_response.status(),
                body::Body::Bytes(get_issue_response.body().await.unwrap()),
            );
        }

        let body_bytes = get_issue_response.body().await;
        let body: Value = serde_json::from_slice(&body_bytes.unwrap()).unwrap();
        let mut issue_labels: Vec<&str> = body["labels"]
            .as_array()
            .unwrap()
            .iter()
            .map(|label_title| label_title.as_str().unwrap())
            .filter(|label_title| label_title != &"merge request")
            .collect();
        issue_labels.push("To test");

        let encoded: String = form_urlencoded::Serializer::new(String::new())
            .append_pair("labels", &issue_labels.join(","))
            .finish();

        let mut update_issue_response = client
            .put(format!(
                "{}/api/v4/projects/{}/issues/{}?private_token={}&{}",
                self.config.gitlab_base_url,
                project_id,
                issue_number,
                self.config.gitlab_access_token,
                encoded
            ))
            .header("Content-Type", "application/json")
            .send()
            .await
            .unwrap();

        if update_issue_response.status().is_client_error()
            || update_issue_response.status().is_server_error()
        {
            error!(
                "A {:?} http code error occur to update issue from merge request",
                update_issue_response.status()
            );
            return HttpResponse::with_body(
                update_issue_response.status(),
                body::Body::Bytes(update_issue_response.body().await.unwrap()),
            );
        }
        HttpResponse::Ok().finish()
    }

    async fn update_merge_request(
        &self,
        client: Client,
        project_id: i64,
        merge_request_iid: i64,
    ) -> HttpResponse {
        if self.extract_new_labels_titles().contains(&"merge request") {
            self.fetch_random_reviewer(&client, project_id, merge_request_iid)
                .await
        }
        if self.json["changes"]
            .as_object()
            .unwrap()
            .contains_key("labels")
        {
            return self.update_related_issue(client, project_id).await;
        }
        HttpResponse::Ok().finish()
    }

    async fn update_issue(&self, client: Client, project_id: i64, issue_iid: i64) -> HttpResponse {
        if self.json["changes"]
            .as_object()
            .unwrap()
            .contains_key("labels")
        {
            return self
                .update_related_merge_request(client, project_id, issue_iid)
                .await;
        }
        HttpResponse::Ok().finish()
    }

    async fn fetch_random_reviewer(
        &self,
        client: &Client,
        merge_request_iid: i64,
        project_id: i64,
    ) {
        let mut merge_request_response = client
            .get(format!(
                "{}/api/v4/projects/{}/merge_requests/{}?private_token={}",
                self.config.gitlab_base_url,
                project_id,
                merge_request_iid,
                self.config.gitlab_access_token
            ))
            .send()
            .await
            .unwrap();

        if merge_request_response.status().is_client_error()
            || merge_request_response.status().is_server_error()
        {
            error!(
                "A {:?} http code error occur on assign a user to a merge request on gitlab",
                merge_request_response.status()
            );
        }
        let merge_request: Value =
            serde_json::from_slice(&merge_request_response.body().await.unwrap()).unwrap();
        if merge_request["assignees"].as_array().is_none()
        {
            return;
        }
        let sql_query = SqlQuery::new(&self.config.postgres_pool);
        let roles = self.get_roles_from_project();
        let gitlab_user = sql_query
            .get_random_user(
                &roles,
                self.json["object_attributes"]["author_id"]
                    .as_u64()
                    .unwrap()
                    .try_into()
                    .unwrap(),
            )
            .await;
        if gitlab_user.is_err() {
            error!("Unable to get gitlab user on database to fetch random reviewer");
            return;
        }
        let gitlab_user = gitlab_user.unwrap();
        if let Some(slack_id) = gitlab_user.slack_id {
            if let Some(slack_name) = gitlab_user.slack_name {
                let slack_query =
                    SlackQuery::new(&self.config.slack_base_url, &self.config.slack_access_token);
                slack_query
                    .ping_user_for_new_merge_request(
                        self.json["object_attributes"]["url"].as_str().unwrap(),
                        &slack_id,
                        &slack_name,
                    )
                    .await;
            }
        }
        let resp = client
            .put(format!(
                "{}/api/v4/projects/{}/merge_requests/{}?private_token={}",
                self.config.gitlab_base_url,
                project_id,
                merge_request_iid,
                self.config.gitlab_access_token
            ))
            .header("Content-Type", "application/json")
            .send_body(json!({"assignee_id": gitlab_user.gitlab_id}))
            .await
            .unwrap();

        if resp.status().is_client_error() || resp.status().is_server_error() {
            error!(
                "A {:?} http code error occur on assign a user to a merge request on gitlab",
                resp.status()
            );
        }
    }

    async fn update_related_merge_request(
        &self,
        client: Client,
        project_id: i64,
        issue_iid: i64,
    ) -> HttpResponse {
        let issue_labels = self.extract_new_labels_titles();

        let mut get_merge_requests_response = client
            .get(format!(
                "{}/api/v4/projects/{}/issues/{}/related_merge_requests?private_token={}",
                self.config.gitlab_base_url, project_id, issue_iid, self.config.gitlab_access_token
            ))
            .header("Content-Type", "application/json")
            .send()
            .await
            .unwrap();

        if get_merge_requests_response.status().is_client_error()
            || get_merge_requests_response.status().is_server_error()
        {
            error!(
                "A {:?} http code error occur to get merge requests related to issue",
                get_merge_requests_response.status()
            );
            return HttpResponse::with_body(
                get_merge_requests_response.status(),
                body::Body::Bytes(get_merge_requests_response.body().await.unwrap()),
            );
        }
        let body_bytes = get_merge_requests_response.body().await;
        let body: Value = serde_json::from_slice(&body_bytes.unwrap()).unwrap();
        let merge_requests: Vec<Value> = body.as_array().unwrap().to_vec();
        for merge_request in merge_requests {
            let merge_request_labels = merge_request["labels"].as_array().unwrap();
            if merge_request_labels.len() == issue_labels.len()
                && merge_request_labels
                    .iter()
                    .zip(&issue_labels)
                    .filter(|(a, b)| a == b)
                    .count()
                    == issue_labels.len()
            {
                return HttpResponse::Ok().finish();
            }
            let encoded: String = form_urlencoded::Serializer::new(String::new())
                .append_pair("labels", &issue_labels.join(","))
                .finish();

            let mut update_merge_request_response = client
                .put(format!(
                    "{}/api/v4/projects/{}/merge_requests/{}?private_token={}&{}",
                    self.config.gitlab_base_url,
                    project_id,
                    merge_request["iid"].as_i64().unwrap(),
                    self.config.gitlab_access_token,
                    encoded
                ))
                .header("Content-Type", "application/json")
                .send()
                .await
                .unwrap();

            if update_merge_request_response.status().is_client_error()
                || update_merge_request_response.status().is_server_error()
            {
                error!(
                    "A {:?} http code error occur to update merge requests related to issue",
                    update_merge_request_response.status()
                );
                return HttpResponse::with_body(
                    update_merge_request_response.status(),
                    body::Body::Bytes(update_merge_request_response.body().await.unwrap()),
                );
            }
        }
        HttpResponse::Ok().finish()
    }

    async fn update_related_issue(&self, client: Client, project_id: i64) -> HttpResponse {
        let issue_number = self.extract_issue_number_from_description();
        let mut get_issue_response = client
            .get(format!(
                "{}/api/v4/projects/{}/issues/{}?private_token={}",
                self.config.gitlab_base_url,
                project_id,
                issue_number,
                self.config.gitlab_access_token
            ))
            .header("Content-Type", "application/json")
            .send()
            .await
            .unwrap();

        if get_issue_response.status().is_client_error()
            || get_issue_response.status().is_server_error()
        {
            error!(
                "A {:?} http code error occur to get issues related to merge request",
                get_issue_response.status()
            );
            return HttpResponse::with_body(
                get_issue_response.status(),
                body::Body::Bytes(get_issue_response.body().await.unwrap()),
            );
        }

        let body_bytes = get_issue_response.body().await;
        let body: Value = serde_json::from_slice(&body_bytes.unwrap()).unwrap();
        let issue_labels: Vec<&str> = body["labels"]
            .as_array()
            .unwrap()
            .iter()
            .map(|label_title| label_title.as_str().unwrap())
            .collect();
        let merge_request_labels = self.extract_new_labels_titles();
        if merge_request_labels.len() == issue_labels.len()
            && merge_request_labels
                .iter()
                .zip(&issue_labels)
                .filter(|(a, b)| a == b)
                .count()
                == issue_labels.len()
        {
            return HttpResponse::Ok().finish();
        }
        let encoded: String = form_urlencoded::Serializer::new(String::new())
            .append_pair("labels", &merge_request_labels.join(","))
            .finish();

        let mut update_issue_response = client
            .put(format!(
                "{}/api/v4/projects/{}/issues/{}?private_token={}&{}",
                self.config.gitlab_base_url,
                project_id,
                issue_number,
                self.config.gitlab_access_token,
                encoded
            ))
            .header("Content-Type", "application/json")
            .send()
            .await
            .unwrap();

        if update_issue_response.status().is_client_error()
            || update_issue_response.status().is_server_error()
        {
            error!(
                "A {:?} http code error occur to update issue related to merge request",
                update_issue_response.status()
            );
            return HttpResponse::with_body(
                update_issue_response.status(),
                body::Body::Bytes(update_issue_response.body().await.unwrap()),
            );
        }

        HttpResponse::Ok().finish()
    }

    async fn open_merge_request(
        &self,
        client: Client,
        project_id: i64,
        merge_request_iid: i64,
    ) -> HttpResponse {
        let description = self.json["object_attributes"]["description"]
            .as_str()
            .unwrap()
            .replace("Closes", "Related to");
        let title = self.json["object_attributes"]["title"]
            .as_str()
            .unwrap()
            .replace("Draft: ", "");
        let mut resp = client
            .put(format!(
                "{}/api/v4/projects/{}/merge_requests/{}?private_token={}",
                self.config.gitlab_base_url,
                project_id,
                merge_request_iid,
                self.config.gitlab_access_token
            ))
            .header("Content-Type", "application/json")
            .send_body(json!({ "description": description, "title": title }))
            .await
            .unwrap();
        if resp.status().is_client_error() || resp.status().is_server_error() {
            error!(
                "A {:?} http code error occur to update issue related to merge request",
                resp.status()
            );
            return HttpResponse::with_body(
                resp.status(),
                body::Body::Bytes(resp.body().await.unwrap()),
            );
        }
        HttpResponse::Ok().finish()
    }

    fn get_roles_from_project(&self) -> [String; 2] {
        let project_name = self.json["project"]["name"].as_str().unwrap();
        if project_name == "web-app-frontend" || project_name == "frontend" {
            [String::from("fullstack"), String::from("frontend")]
        } else {
            [String::from("fullstack"), String::from("backend")]
        }
    }

    fn extract_project_id(&self) -> i64 {
        self.json["project"]["id"].as_i64().unwrap()
    }

    fn extract_merge_request_iid(&self) -> i64 {
        self.json["object_attributes"]["iid"].as_i64().unwrap()
    }

    fn extract_issue_iid(&self) -> i64 {
        self.json["object_attributes"]["iid"].as_i64().unwrap()
    }

    fn extract_issue_number_from_description(&self) -> i64 {
        let description = self.json["object_attributes"]["description"]
            .as_str()
            .unwrap();
        let re = Regex::new(r"(?:#)(\w+)\b").unwrap();
        let result = re.captures(description).unwrap();
        result[1].parse::<i64>().unwrap()
    }

    fn extract_new_labels_titles(&self) -> Vec<&str> {
        let current_labels = self.json["changes"]["labels"]["current"].as_array();
        match current_labels {
            Some(x) => x
                .iter()
                .map(|label| label["title"].as_str().unwrap())
                .collect(),
            None => vec![],
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use sqlx::postgres::PgPool;
    #[tokio::test]
    async fn test_extract_new_labels_titles() {
        let app_config = AppConfig {
            gitlab_access_token: String::from("mock_gitlab_access_token"),
            gitlab_base_url: String::from("mock_gitlab_base_url"),
            slack_base_url: String::from("mock_slack_base_url"),
            slack_access_token: String::from("mock_slack_access_token"),
            postgres_pool: PgPool::builder()
                .max_size(5)
                .build("mock_database_url")
                .await
                .unwrap(),
        };
        let config = web::Data::new(app_config);
        let value = json!({
            "object_kind": "merge_request",
            "changes": {
                "labels": {
                    "current": [
                        {"title": "Merge Request"}, {"title": "Bug"}, {"title": "Important"}
                    ]
                }
            }
        });
        let obj_json: Map<String, Value> = value.as_object().unwrap().clone();
        let action = Action::new(&obj_json, &config);
        assert_eq!(
            action.extract_new_labels_titles(),
            vec!["Merge Request", "Bug", "Important"]
        );
    }
    #[tokio::test]
    async fn test_extract_new_labels_titles_with_empty_labels() {
        let app_config = AppConfig {
            gitlab_access_token: String::from("mock_gitlab_access_token"),
            gitlab_base_url: String::from("mock_gitlab_base_url"),
            slack_base_url: String::from("mock_slack_base_url"),
            slack_access_token: String::from("mock_slack_access_token"),
            postgres_pool: PgPool::builder()
                .max_size(5)
                .build("mock_database_url")
                .await
                .unwrap(),
        };
        let config = web::Data::new(app_config);
        let value = json!({
            "object_kind": "merge_request",
            "changes": {
                "labels": {
                    "current": []
                }
            }
        });
        let obj_json: Map<String, Value> = value.as_object().unwrap().clone();
        let action = Action::new(&obj_json, &config);
        let result: Vec<&str> = vec![];
        assert_eq!(action.extract_new_labels_titles(), result);
    }

    #[tokio::test]
    async fn test_extract_issue_number_from_description() {
        let app_config = AppConfig {
            gitlab_access_token: String::from("mock_gitlab_access_token"),
            gitlab_base_url: String::from("mock_gitlab_base_url"),
            slack_base_url: String::from("mock_slack_base_url"),
            slack_access_token: String::from("mock_slack_access_token"),
            postgres_pool: PgPool::builder()
                .max_size(5)
                .build("mock_database_url")
                .await
                .unwrap(),
        };
        let config = web::Data::new(app_config);
        let value = json!({
            "object_kind": "issue",
            "object_attributes": {
                "description": "Closes #13"
            }
        });
        let obj_json: Map<String, Value> = value.as_object().unwrap().clone();
        let action = Action::new(&obj_json, &config);
        assert_eq!(action.extract_issue_number_from_description(), 13);
    }
}
