use sqlx::types::Uuid;

#[derive(Debug)]
pub enum ObjectKind {
    MergeRequest,
    Issue,
    Unknown
}

pub trait Modify {
    fn send(&self);
}

#[derive(sqlx::Type)]
#[sqlx(rename = "role", rename_all = "lowercase")]
pub enum Role {
    FrontEnd,
    BackEnd,
    FullStack
}

#[derive(sqlx::FromRow)]
pub struct GitlabUser {
    pub uuid: Uuid,
    pub gitlab_id: i32,
    pub name: String,
    pub slack_id: Option<String>,
    pub slack_name: Option<String>,
    pub role: Role
}
