use std::env;
use actix_web::{middleware, web, App, HttpServer};
use listenfd::ListenFd;
use refinery::{
    config::Config,
};
mod core;

mod embedded {
    use refinery::embed_migrations;
    embed_migrations!("migrations");
}

async fn migrate() {
    let mut db_config = Config::from_env_var("postgres_pool").unwrap();
    let runner = embedded::migrations::runner();

    runner.run_async(&mut db_config).await.unwrap();
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    let mut listenfd = ListenFd::from_env();
    let config = core::config::AppConfig::new().await;
    migrate().await;
    let mut server = HttpServer::new(move || {
        App::new()
            .wrap(middleware::Logger::default())
            .wrap(middleware::Logger::new("%a %{User-Agent}i"))
            .data(config.clone())
            .service(
                web::scope("/v1/triage")
                    .service(core::routes::gitlab_webhook)
                    .service(core::routes::health_check),
            )
    });
    server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        server.listen(l)?
    } else {
        server.bind("0.0.0.0:6000")?
    };

    server.run().await
}
