create table users (
  uuid uuid primary key,
  gitlab_id integer not null,
  name varchar(255) not null,
  role role not null
);